from django.urls import path
from rest_framework.routers import DefaultRouter

from .views import ArticleView, GenericArticleView, SingleArticleView, SetArticleView, ArticleSetModelView

app_name = 'articles'

# urlpatterns = [
#     # path('articles/', GenericArticleView.as_view()),
#     # path('articles/<int:pk>', SingleArticleView.as_view())
#     path('articles/', SetArticleView.as_view({'get':'list'})),
#     path('articles/<int:pk>', SetArticleView.as_view({'get': 'retrieve'})),
# ]

router = DefaultRouter()
router.register(r'articles', ArticleSetModelView, basename='article')

urlpatterns = router.urls